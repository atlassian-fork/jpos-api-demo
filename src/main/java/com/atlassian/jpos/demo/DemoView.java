package com.atlassian.jpos.demo;

import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.rm.portfolio.publicapi.interfaces.plan.PlanAPI;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class DemoView extends JiraWebActionSupport {

    private final PlanAPI planApi;

    @Autowired
    public DemoView(@ComponentImport PlanAPI planApi) {
      this.planApi = planApi;
    }

    @Override
    public String execute() throws Exception {
        return "success";
    }

    @SuppressWarnings("unused")
    public String getResult() {
        try {
            long numPlans = planApi.count();
            return numPlans + " Plan" + (numPlans == 1 ? "" : "s");
        } catch (Exception e) {
            return "error: " + e.getMessage();
        }
	  }
}

