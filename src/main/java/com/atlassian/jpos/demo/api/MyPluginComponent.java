package com.atlassian.jpos.demo.api;

public interface MyPluginComponent
{
    String getName();
}